# Docker sandbox VMs

Dockerfile and a python script to create multiple ubuntu sandbox vm's for students to mess with

Build docker image
Make sure you are located in the same folder as the Dockerfile and run the following command
```bash
sudo docker build . -t ssh-image
```

Populate your users file with the usernames you want to give your containers one per line

Run container creation script
```bash
sudo python3 createContainers.py
```

## Users
A container will be created for each username included in the users.txt 
the output file usersNew.txt will contain the usernames, passwords and ports of the created containers in a csv (Comma seperated values) file

Example:
```
Username,Password,Port,Date
adc,20EYPUle,53001,2021-01-29
aj,9c2jWrII,53002,2021-01-29
```
## Connecting to a container
In order to connect to a container the users can use the following command in any ssh terminal
```bash
ssh "username"@"hostname" -p "port"
```
Example for tskel
```bash
ssh adc@tskel -p 52025
```

### Using Putty
When using putty you simply just add "tskel" as the host
your port as the port and then specify your credentials when asked by the putty console

# Docker management
To see running containers on the host system use 
```bash
sudo docker ps
```
To kill a specific container (it is not necessary to use the entire id just a unique part)
```bash
sudo docker kill "container id"
```
To remove all unused/stopped containers from the system
```bash
sudo docker container prune
```
To build/rebuild the image (-t is used to specify the image name, usually the path is "." for the folder you are in)
```bash
sudo docker build "path to Dockerfile" -t ssh-image
```
