FROM ubuntu:20.04

RUN apt-get update && apt-get install -y openssh-server && apt-get install sudo
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
COPY createUsers.sh .
RUN chmod 777 createUsers.sh
EXPOSE 22
CMD ["./createUsers.sh"]
