# build the docker image before running this script
# docker build . -t ssh-image
# its important to give the image the name ssh-image

# make sure the csv file with the usernames is located in the same 
# folder and has the filename users.txt

# importing the csv file with the users
import secrets
import string
import subprocess
from datetime import date

class User:
    def __init__(self, name):
        self.name = name.rstrip('\n')
        self.password = None
        self.port = None
    def __str__(self):
        return "Username: {0}\nPassword: {1}\nPort:{2}".format(self.name, self.password, self.port)

def create_container(user: User):
    command = "docker run -d -p {0}:22 --env username={1} --env password={2} --name {1} ssh-image".format(user.port, user.name, user.password)
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

def write_user_to_file(user: User):
    today = date.today()
    with open('usersNew.txt', 'a') as output_file:
        output_file.write("{0},{1},{2},{3}\n".format(user.name, user.password, user.port, today))
        output_file.close()

users = []

users_file = open('users.txt', 'r')
usernames = users_file.readlines()

alphabet = string.ascii_letters + string.digits
initial_port = 53000
for username in usernames:
    user = User(username)
    user.password = ''.join(secrets.choice(alphabet) for i in range(8)) # creating an 8 character password
    user.port = initial_port + 1
    initial_port += 1
    users.append(user)

for user in users:
    write_user_to_file(user)
    create_container(user)